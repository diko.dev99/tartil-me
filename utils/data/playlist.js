export const fields = [
  { id: 1, key: 'sura_name', thClass: 'text-secondary small font-weight-bold' },
  { id: 2, key: 'reciter', thClass: 'text-secondary small font-weight-bold' },
  { id: 3, key: 'rewayat', thClass: 'text-secondary small font-weight-bold' },
  { id: 4, key: 'duration', thClass: 'text-secondary small font-weight-bold' }
]

export const items = [
  {
    id: 1,
    sura_name: { first: 'Al-Fatihah', last: '(The Table Spread)' },
    reciter: 'Abdallah Kamel',
    rewayat: 'Hafs An Assem',
    duration: '0:00:42'
  },
  {
    id: 2,
    sura_name: { first: 'Al-Maidah', last: '(The Table Spread)' },
    reciter: 'Abdallah Kamel',
    rewayat: 'Hafs An Assem',
    duration: '0:00:42'
  },
  {
    id: 3,
    sura_name: { first: 'Al-Maidah', last: '(The Table Spread)' },
    reciter: 'Abdallah Kamel',
    rewayat: 'Hafs An Assem',
    duration: '0:00:42'
  },
  {
    id: 4,
    sura_name: { first: 'Al-Maidah', last: '(The Table Spread)' },
    reciter: 'Abdallah Kamel',
    rewayat: 'Hafs An Assem',
    duration: '0:00:42'
  },
  {
    id: 5,
    sura_name: { first: 'Al-Maidah', last: '(The Table Spread)' },
    reciter: 'Abdallah Kamel',
    rewayat: 'Hafs An Assem',
    duration: '0:00:42'
  }
]

export const fieldsInDetailQori = [
  { id: 1, key: 'sura_name', thClass: 'text-secondary small font-weight-bold' },
  { id: 2, key: 'duration', thClass: 'text-secondary small font-weight-bold' }
]

export const itemsInDetailQori = [
  {
    id: 1,
    sura_name: { first: 'Al-Fatihah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 2,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 3,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 4,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 5,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 6,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 7,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 8,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  },
  {
    id: 9,
    sura_name: { first: 'Al-Maidah', last: 'The Table Spread' },
    duration: '0:00:42'
  }

]
