export const menu = [
  {
    id: 1,
    title: 'Home',
    icon: '-house-door',
    child: false,
    to: '/'
  },
  {
    id: 2,
    title: 'Favorite',
    icon: 'Home',
    child: false,
    to: '/favorite'
  }, {
    id: 3,
    title: 'Reciter',
    icon: 'Home',
    child: false,
    to: '/reciter'
  }, {
    id: 4,
    title: 'Al-Quran',
    icon: 'Home',
    child: false,
    to: '/quran'
  }, {
    id: 5,
    title: 'Playlist',
    icon: 'Home',
    child: true,
    to: '/playlist'
  }
]

export const shareTo = [
  { id: 1, title: 'Facebook', images: 'icons-wa.png', hr: true },
  { id: 2, title: 'Instagram', images: 'icons-ig.png', hr: true },
  { id: 3, title: 'Telegram', images: 'icons-tg.png', hr: true },
  { id: 4, title: 'Twitter', images: 'icons-twitter.png', hr: true },
  { id: 5, title: 'Whatsapp', images: 'icons-wa.png', hr: true },
  { id: 6, title: 'Copy Link', images: 'icons-link.png', hr: false }
]
