import { menu, shareTo } from '../utils/const.js'
import {
  fields,
  items,
  fieldsInDetailQori,
  itemsInDetailQori
} from '../utils/data/playlist'

export const state = () => ({
  items: [],
  fields: []
})

export const getters = {
  fields () {
    return fields
  },
  items () {
    return items
  },
  menu () {
    return menu
  },
  shareTo () {
    return shareTo
  },
  fieldsInDetailQori () {
    return fieldsInDetailQori
  },
  itemsInDetailQori () {
    return itemsInDetailQori
  }
}
